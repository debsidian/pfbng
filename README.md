# pfBlockerng

These files are intended to serve as supplemental lists for your [pfSense](https://www.pfsense.org/) firewall or [Pihole](https://pi-hole.net/). These lists are most effective if used as an adjunct to the following actively maintained DNS Blocklists: [StevenBlack Unified Hosts](https://github.com/StevenBlack/hosts), [1hosts Lite](https://github.com/badmojr/1Hosts) and [lightswitch05 Ads & Tracking](https://github.com/lightswitch05/hosts). 

There should be only minimal negative impact on your browsing experience from the DNS lists in this repository. 

The IP address lists may cause some things to break if they use Google, Cloudflare or Quad9's DoH implementations. 
